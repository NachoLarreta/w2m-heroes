# Proyecto heroes 
_Es un proyecto a nivel test de entrevista para w2m. Tener en cuenta que el backend esta solo hecho con fines de brindar una simulacion de datos. No esta hecho con buenas practicas ni con consideracion a ser evaluado._
## Comenzando 🚀
_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._
### Pre-requisitos 📋
_NodeJS y AngularCli_
```
NodeJS version v14.17.3 o latest
Angular Cli Version 12.1.3 o latest
```
### Instalación 🔧
Front-end
```sh
npm install
```
Back-end Mock
```sh
cd server
npm install
```
### Iniciando la aplicacion 🏎 
_Debemos correr el siguiente comando_ 

Front-End
```sh
npm run start
```
Back-End Mock
```sh
cd server
npm run start
```
_Nota: antes de iniciar la aplicacion deberiamos iniciar el servidor backend._

## Ejecutando las pruebas 🧪
```sh
npm run test
```
## Despliegue 📦

_Para empaquetar la aplicacion_
```sh
npm run build
```
