export class List<T> {

    elements: Array<T>;
    length: number;
    pageSize: number;
    pageIndex: number;

    constructor(length?: number, pageSize?: number, pageIndex?: number, elements?: Array<T>) {
        this.elements = elements;
        this.length = length;
        this.pageSize = pageSize;
        this.pageIndex = pageIndex;
    }

}