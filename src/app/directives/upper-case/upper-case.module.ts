import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UpperCaseDirective } from './upper-case.directive';

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        UpperCaseDirective
    ],
    exports: [
        UpperCaseDirective
    ]
})
export class UpperCaseModule { }
