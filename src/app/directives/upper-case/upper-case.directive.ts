import { Directive } from "@angular/core";

@Directive({
    selector: 'input[upper-case], input[upperCase]',
    host: {
        '(keyup)': 'keyUp($event)'
    }
})
export class UpperCaseDirective {

    keyUp($event) {
        $event.currentTarget.value = $event.currentTarget.value.toUpperCase();
    }

}