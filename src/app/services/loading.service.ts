import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class LoadingService {

  loading: BehaviorSubject<boolean>;
  loadingMap: Map<string, boolean>;

  constructor() { 
    this.loading = new BehaviorSubject<boolean>(false);
    this.loadingMap = new Map<string, boolean>();
  }

  setLoading(loading: boolean, url: string): void {
    if (!url) return;
    if (loading === true) {
      this.loadingMap.set(url, loading);
      this.loading.next(true);
      return;
    }
    if (loading === false && this.loadingMap.has(url)) this.loadingMap.delete(url);
    if (this.loadingMap.size === 0) this.loading.next(false);
  }
}