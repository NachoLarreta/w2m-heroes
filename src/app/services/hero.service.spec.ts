import { TestBed } from '@angular/core/testing';
import { Hero, List } from '@models';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HeroService } from '@services';
import { environment } from 'src/environments/environment';

describe('HeroService', () => {

  let httpTestingController: HttpTestingController;
  let service: HeroService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HeroService]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(HeroService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be get all the heroes', (done) => {
    const mockHeroes = {
      elements: [
        {
          id: '1234',
          name: 'Test'
        }
      ],
      length: 10,
      pageSize: 5,
      pageIndex: 1
    };
    service.getAll().subscribe(response => {
      const isAnArrayOfHeroes: boolean = Array.isArray(response.elements) && response.elements.every(hero => hero instanceof Hero);
      expect(isAnArrayOfHeroes).toEqual(true);
      const isAListOfHeroes: boolean = response instanceof List;
      expect(isAListOfHeroes).toEqual(true);
      done();
    });
    const req = httpTestingController.expectOne(`${environment.api}?pageSize=5&pageIndex=1`);
    req.flush(mockHeroes);
  });

  it('should be get hero by id', (done) => {
    const mockHeroe = {
      id: '1234',
      name: 'Test'
    };
    service.getById('1234').subscribe(hero => {
      expect(hero instanceof Hero).toEqual(true);
      done();
    });
    const req = httpTestingController.expectOne(`${environment.api}?id=1234`);
    req.flush(mockHeroe);
  });

  it('should be get hero by name filter', (done) => {
    const mockHeroes = {
      elements: [
        {
          id: '1234',
          name: 'Test'
        }
      ],
      length: 10,
      pageSize: 5,
      pageIndex: 1
    };
    service.getFilteredByName('test').subscribe(response => {
      const isAnArrayOfHeroes: boolean = Array.isArray(response.elements) && response.elements.every(hero => hero instanceof Hero);
      expect(isAnArrayOfHeroes).toEqual(true);
      const isAListOfHeroes: boolean = response instanceof List;
      expect(isAListOfHeroes).toEqual(true);
      done();
    });
    const req = httpTestingController.expectOne(`${environment.api}?name=test&pageSize=5&pageIndex=1`);
    req.flush(mockHeroes);
  });

  it('should be delete a hero', (done) => {
    service.delete('1234').subscribe(() => {
      done();
    });
    const req = httpTestingController.expectOne(`${environment.api}?id=1234`);
    req.flush(null);
  });

  it('should be update a hero', (done) => {
    service.update(new Hero('1234', 'test')).subscribe(() => {
      done();
    })
    const req = httpTestingController.expectOne(`${environment.api}`);
    req.flush(null);
  });

  it('should be create a hero', (done) => {
    service.add(new Hero(null, 'test')).subscribe(hero => {
      expect(hero instanceof Hero).toEqual(true);
      expect(hero.id).toEqual('1234');
      done();
    })
    const req = httpTestingController.expectOne(`${environment.api}`);
    req.flush({id: '1234', name: 'test'});
  });

});
