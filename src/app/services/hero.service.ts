import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Hero, List } from '@models';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable()
export class HeroService {

  constructor(private http: HttpClient) { 
  }

  add(hero: Hero): Observable<Hero> {
    return this.http.post<Hero>(environment.api, hero)
      .pipe(map(response => response ? new Hero(response.id, response.name) : null));
  }

  getAll(pageSize: number = 5, pageIndex: number = 1): Observable<List<Hero>> {
    return this.http.get<List<Hero>>(`${environment.api}?pageSize=${pageSize}&pageIndex=${pageIndex}`)
      .pipe(
        map(response => response ? 
            new List<Hero>(response.length, response.pageSize, response.pageIndex, response.elements ? response.elements.map(hero => new Hero(hero.id, hero.name)) : new Array<Hero>())
            : new List<Hero>()
          )
      );
  }

  getById(id: string): Observable<Hero> {
    return this.http.get<Hero>(`${environment.api}?id=${id}`)
      .pipe(map(response => response ? new Hero(response.id, response.name) : null));
  }

  getFilteredByName(name: string, pageSize: number = 5, pageIndex: number = 1): Observable<List<Hero>>  {
    return this.http.get<List<Hero>>(`${environment.api}?name=${name}&pageSize=${pageSize}&pageIndex=${pageIndex}`)
      .pipe(
        map(response => response ? 
            new List<Hero>(response.length, response.pageSize, response.pageIndex, response.elements ? response.elements.map(hero => new Hero(hero.id, hero.name)) : new Array<Hero>())
            : new List<Hero>()
          )
      );
  }

  update(hero: Hero): Observable<void> {
    return this.http.put<void>(environment.api, hero);
  }

  delete(id: string): Observable<void> {
    return this.http.delete<void>(`${environment.api}?id=${id}`);
  }

}
