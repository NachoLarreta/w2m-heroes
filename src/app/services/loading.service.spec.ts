import { TestBed } from '@angular/core/testing';
import { LoadingService } from './loading.service';

describe('LoadingService', () => {

    let service: LoadingService;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            providers: [LoadingService]
        });
        service = TestBed.inject(LoadingService);
        
    });

    it('To be created', () => {
        expect(service).toBeTruthy();
    });

    it('setLoading', () => {
        expect(service).toBeTruthy();
        const spyOnLoadingNext = spyOn(service.loading, 'next').and.callFake(() => {});
        const urlTest: string = 'http://localhost:3000';
        const urlTest2: string = 'http://localhost:3001';
        const urlTest3: string = 'http://localhost:3003';
        service.setLoading(true, urlTest);
        expect(service.loadingMap.get(urlTest)).toEqual(true);
        expect(spyOnLoadingNext).toHaveBeenCalled();
        spyOnLoadingNext.calls.reset();
        service.setLoading(false, urlTest);
        expect(spyOnLoadingNext).toHaveBeenCalled();
        expect(service.loadingMap.get(urlTest)).toEqual(undefined);
        spyOnLoadingNext.calls.reset();
        service.setLoading(true, null);
        expect(spyOnLoadingNext).not.toHaveBeenCalled();
        service.setLoading(true, urlTest);
        service.setLoading(true, urlTest2);
        spyOnLoadingNext.calls.reset();
        service.setLoading(false, urlTest);
        service.setLoading(false, urlTest3);
        expect(spyOnLoadingNext).not.toHaveBeenCalled();
        service.setLoading(false, urlTest2);
        expect(spyOnLoadingNext).toHaveBeenCalled();
    });

});
