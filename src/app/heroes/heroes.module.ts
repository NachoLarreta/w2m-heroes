import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent, DeleteConfirmationComponent } from '@heroes';
import { HeroesRoutingModule } from './heroes-routing.module';
import { HeroService } from '@services';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  imports: [
    CommonModule,
    HeroesRoutingModule,
    MatSnackBarModule,
    MatTableModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    FormsModule,
    MatPaginatorModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatDialogModule
  ],
  declarations: [
    ListComponent,
    DeleteConfirmationComponent
  ],
  providers: [
    HeroService
  ]
})
export class HeroesModule { }
