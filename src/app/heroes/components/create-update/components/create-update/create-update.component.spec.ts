import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateUpdateModule } from '../../create-update.module';
import { CreateUpdateComponent } from './create-update.component';

describe('CreateUpdateComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        CreateUpdateModule
      ],
      declarations: [
        CreateUpdateComponent
      ]
    }).compileComponents();
  });

  it('should create the create update', () => {
    const fixture = TestBed.createComponent(CreateUpdateComponent);
    const createUpdate = fixture.componentInstance;
    expect(createUpdate).toBeTruthy();
  });
  
});
