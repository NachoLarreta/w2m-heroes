import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Hero } from '@models';
import { HeroService } from '@services';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'create-update',
    templateUrl: 'create-update.component.html',
    styleUrls: ['create-update.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateUpdateComponent implements OnInit, OnDestroy {

    editMode: boolean;
    form: FormGroup;
    error: boolean;
    private hero: Hero;
    private ngUnsubscribe: Subject<void>;

    constructor(private heroService: HeroService, private snackBar: MatSnackBar,
        private activatedRoute: ActivatedRoute, private router: Router) {
        this.ngUnsubscribe = new Subject<void>();
    }

    ngOnInit() {
        this.initForm();
        const id: string = this.activatedRoute.snapshot.params['id'];
        this.editMode = !!id;
        if (!id) return;
        this.heroService.getById(id)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(hero => this.onGetHero(hero), error => this.onFetchDataError(error));
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    private onGetHero(hero: Hero) {
        this.hero = hero;
        this.form.controls.name.setValue(this.hero.name);
        this.error = false;
    }

    initForm() {
        this.form = new FormBuilder().group({
            name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]]
        });
    }

    private onFetchDataError(error) {
        console.error(error);
        const snackBar = this.snackBar.open('Error fetching heroe', 'Retry');
        snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => {
            snackBar.dismiss();
            location.reload();
        });
        this.error = true;
    }

    confirm() {
        if (!this.form.valid) return;
        if (this.editMode) {
            this.hero.name = this.form.controls.name.value;
            this.heroService.update(this.hero)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(() => this.onHeroUpdated(), error => this.onError(error));
            return;
        }
        this.hero = new Hero(null, this.form.controls.name.value);
        this.heroService.add(this.hero)
            .pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => this.onHeroCreated(), error => this.onError(error))
    }

    onHeroUpdated() {
        const snackBar = this.snackBar.open('Hero updated', 'Ok');
        snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => snackBar.dismiss());
        this.goBack();
    }

    onHeroCreated() {
        const snackBar = this.snackBar.open('Hero created', 'Ok');
        snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => snackBar.dismiss());
        this.goBack();
    }

    goBack() {
        this.router.navigate(['..']);
    }

    onError(error) {
        console.error(error);
        if (error.status === 404) {
            const snackBar = this.snackBar.open('Hero not found', 'Ok');
            snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => snackBar.dismiss());
            return;
        } 
        const snackBar = this.snackBar.open('Unexpected error', 'Ok');
        snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => snackBar.dismiss());
    }

}
