import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeroService } from '@services';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateUpdateRoutingModule } from './create-update-routing.module';
import { CreateUpdateComponent } from '@heroes/create-update';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { UpperCaseModule } from '@directives';

@NgModule({
  imports: [
    CommonModule,
    CreateUpdateRoutingModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    FormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    UpperCaseModule
  ],
  declarations: [
    CreateUpdateComponent
  ],
  providers: [
    HeroService
  ]
})
export class CreateUpdateModule { }
