import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateUpdateComponent } from './components/create-update/create-update.component';

const routes: Routes = [
  {
    path: ':id',
    component: CreateUpdateComponent
  },
  {
    path: '',
    component: CreateUpdateComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateUpdateRoutingModule {}
