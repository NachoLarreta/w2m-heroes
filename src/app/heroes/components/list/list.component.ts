import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Hero, List } from '@models';
import { HeroService } from '@services';
import { BehaviorSubject, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, takeUntil } from 'rxjs/operators';
import { DeleteConfirmationComponent } from '../delete-confirmation';

@Component({
    selector: 'list',
    templateUrl: 'list.component.html',
    styleUrls: ['list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnInit, OnDestroy {

    heroes: BehaviorSubject<List<Hero>>;
    displayedColumns: Array<string>;
    filter: FormControl;

    private ngUnsubscribe: Subject<void>;

    constructor(private heroService: HeroService, private snackBar: MatSnackBar,
        private dialog: MatDialog) {
        this.ngUnsubscribe = new Subject<void>();
        this.displayedColumns = new Array<string>();
        this.displayedColumns.push('name');
        this.displayedColumns.push('actions');
        this.filter = new FormControl();
        this.heroes = new BehaviorSubject<List<Hero>>(new List<Hero>(0, 5, 0));
    }

    ngOnInit() {
        this.fetchHeroes();
        this.filter.valueChanges
            .pipe(debounceTime(500), distinctUntilChanged(),
                takeUntil(this.ngUnsubscribe), switchMap(filter => this.heroService.getFilteredByName(filter)))
            .subscribe(heroes => this.heroes.next(heroes), error => this.onGetError(error));
    }

    ngOnDestroy() {
        this.ngUnsubscribe.next();
        this.ngUnsubscribe.complete();
    }

    private fetchHeroes() {
        if (this.filter.value) {
            this.heroService.getFilteredByName(this.filter.value, this.heroes.value.pageSize, this.heroes.value.pageIndex + 1)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(heroes => this.heroes.next(heroes), error => this.onGetError(error));
            return;
        } 
        this.heroService.getAll(this.heroes.value.pageSize, this.heroes.value.pageIndex + 1)
                .pipe(takeUntil(this.ngUnsubscribe))
                .subscribe(heroes => this.heroes.next(heroes), error => this.onGetError(error));
    }

    onPage($event) {
        this.updatePaginationSettings($event.pageSize, $event.pageIndex);
        this.fetchHeroes();
    }

    private updatePaginationSettings(pageSize: number, pageIndex: number) {
        const heroes: List<Hero> = this.heroes.value;
        heroes.pageSize = pageSize;
        heroes.pageIndex = pageIndex;
        this.heroes.next(heroes);
    }

    showConfirmDelte(hero: Hero) {
        const dialogRef = this.dialog.open(DeleteConfirmationComponent, {
            width: '510px',
            data: hero
        });

        dialogRef.afterClosed().subscribe(result => this.onDialogConfirmationClosed(result));
    }

    private onDialogConfirmationClosed(result) {
        if (!result.confirm) return;
        this.heroService.delete(result.hero).pipe(takeUntil(this.ngUnsubscribe))
            .subscribe(() => this.onDelete(), error => this.onDeleteError(error));
    }

    private onDeleteError(error) {
        console.error(error);
        const snackBar = this.snackBar.open('No se pudo eliminar el heroe.', 'Ok');
        snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => snackBar.dismiss());
    }

    private onDelete() {
        const snackBar = this.snackBar.open('Heroe borrado correctamente', 'Ok');
        snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => snackBar.dismiss());
        this.updatePaginationSettings(5, 0);
        this.fetchHeroes();
    }

    private onGetError(error) {
        console.error(error);
        const snackBar = this.snackBar.open('Error fetching heroes', 'Ok');
        snackBar.onAction().pipe(takeUntil(this.ngUnsubscribe)).subscribe(() => snackBar.dismiss());
    }

}
