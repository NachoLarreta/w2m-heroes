import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Type } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { MatInput } from '@angular/material/input';
import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroesModule } from '@heroes';
import { Hero, List } from '@models';
import { HeroService } from '@services';
import { of, throwError } from 'rxjs';
import { ListComponent } from './list.component';

describe('ListComponent', () => {

    let fixture: ComponentFixture<ListComponent>;
    let listComponent: ListComponent;
    let mockHeroService: HeroService;
    let mockDialongService: MatDialog;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                HttpClientTestingModule,
                HeroesModule,
                BrowserAnimationsModule
            ]
        }).compileComponents();
        fixture = TestBed.createComponent(ListComponent);
        listComponent = fixture.componentInstance;
        mockHeroService = fixture.debugElement.injector.get<HeroService>(HeroService as Type<HeroService>);
        mockDialongService = fixture.debugElement.injector.get<MatDialog>(MatDialog as Type<MatDialog>);
        await fixture.detectChanges();
        await fixture.whenStable();
    });

    it('should create the list component', () => {
        expect(listComponent).toBeTruthy();
    });

    it('ngOnInit', () => {
        const spyOnFetchHeroes = spyOn<any>(listComponent, 'fetchHeroes').and.callFake(() => {});
        listComponent.ngOnInit();
        expect(spyOnFetchHeroes).toHaveBeenCalled();
    });

    it('filter changes', async () => {
        //On Success
        const spyOnHeroServicesGetFilterByName = spyOn(mockHeroService, 'getFilteredByName').and.callFake(() => of(new List(1, 1, 0, [new Hero('1234', 'Test')])));
        const spyHeroesNext = spyOn(listComponent.heroes, 'next').and.callFake(() => {});
        const input = fixture.debugElement.query(By.directive(MatInput));
        input.nativeElement.value = 'Test1234';
        input.nativeElement.dispatchEvent(new Event('input'));
        await fixture.detectChanges();
        await fixture.whenStable();
        expect(spyOnHeroServicesGetFilterByName).toHaveBeenCalled();
        expect(spyHeroesNext).toHaveBeenCalled();
        spyHeroesNext.calls.reset();
        spyOnHeroServicesGetFilterByName.calls.reset();
        //On Error
        spyOnHeroServicesGetFilterByName.and.callFake(() => throwError(500));
        input.nativeElement.value = 'Test12';
        input.nativeElement.dispatchEvent(new Event('input'));
        await fixture.detectChanges();
        await fixture.whenStable();
        expect(spyOnHeroServicesGetFilterByName).toHaveBeenCalled();
        expect(spyHeroesNext).not.toHaveBeenCalled();
    });

    it('fetchHeroes', async () => {
        // On Success has filter value
        const spyOnHeroServicesGetFilterByName = spyOn(mockHeroService, 'getFilteredByName').and.callFake(() => of(new List(1, 1, 0, [new Hero('1234', 'Test')])));
        const spyHeroesNext = spyOn(listComponent.heroes, 'next').and.callFake(() => {});
        const input = fixture.debugElement.query(By.directive(MatInput));
        input.nativeElement.value = 'Test1234';
        input.nativeElement.dispatchEvent(new Event('input'));
        await fixture.detectChanges();
        await fixture.whenStable();
        spyOnHeroServicesGetFilterByName.calls.reset();
        spyHeroesNext.calls.reset();
        listComponent['fetchHeroes']();
        expect(spyOnHeroServicesGetFilterByName).toHaveBeenCalled();
        expect(spyHeroesNext).toHaveBeenCalled();
        spyHeroesNext.calls.reset();
        spyOnHeroServicesGetFilterByName.calls.reset();
        // On Error has filter value
        spyOnHeroServicesGetFilterByName.and.callFake(() => throwError(500));
        listComponent['fetchHeroes']();
        expect(spyOnHeroServicesGetFilterByName).toHaveBeenCalled();
        expect(spyHeroesNext).not.toHaveBeenCalled();
        // On Success has not filter in value
        const spyOnHeroServicesGetAll = spyOn(mockHeroService, 'getAll').and.callFake(() => of(new List(1, 1, 0, [new Hero('1234', 'Test')])));
        input.nativeElement.value = '';
        input.nativeElement.dispatchEvent(new Event('input'));
        await fixture.detectChanges();
        await fixture.whenStable();
        spyOnHeroServicesGetAll.calls.reset();
        spyHeroesNext.calls.reset();
        listComponent['fetchHeroes']();
        expect(spyOnHeroServicesGetAll).toHaveBeenCalled();
        expect(spyHeroesNext).toHaveBeenCalled();
        spyHeroesNext.calls.reset();
        spyOnHeroServicesGetAll.calls.reset();
         // On Error has filter value
         spyOnHeroServicesGetAll.and.callFake(() => throwError(500));
         listComponent['fetchHeroes']();
         expect(spyOnHeroServicesGetAll).toHaveBeenCalled();
    });

    it('onPage', () => {
        const spyOnUpdatePaginationSettings = spyOn<any>(listComponent, 'updatePaginationSettings').and.callFake(() => {});
        const spyOnFetchHeroes = spyOn<any>(listComponent, 'fetchHeroes').and.callFake(() => {});
        listComponent.onPage({pageSize: 1, pageIndex: 0});
        expect(spyOnUpdatePaginationSettings).toHaveBeenCalled();
        expect(spyOnFetchHeroes).toHaveBeenCalled();
    });

    it('updatePaginationSettings', () => {
        const spyHeroesNext = spyOn(listComponent.heroes, 'next').and.callThrough();
        listComponent['updatePaginationSettings'](1, 0);
        expect(listComponent.heroes.value.pageIndex).toEqual(0);
        expect(listComponent.heroes.value.pageSize).toEqual(1);
        expect(spyHeroesNext).toHaveBeenCalled();
    });

    it('showConfirmDelte', () => {
        const spyDialogOpen = spyOn(mockDialongService, 'open').and.callThrough();
        listComponent.showConfirmDelte(new Hero('1234', 'Test'));
        expect(spyDialogOpen).toHaveBeenCalled();
    });

    it('onDialogConfirmationClosed', () => {
        const spyDelete= spyOn(mockHeroService, 'delete').and.callFake(() => of(null)); 
        const spyOnDelete = spyOn<any>(listComponent, 'onDelete').and.callThrough();
        const spyOnDeleteError = spyOn<any>(listComponent, 'onDeleteError').and.callThrough();
        listComponent['onDialogConfirmationClosed']({confirm: false});
        expect(spyDelete).not.toHaveBeenCalled();
        listComponent['onDialogConfirmationClosed']({confirm: true});
        expect(spyDelete).toHaveBeenCalled();
        expect(spyOnDelete).toHaveBeenCalled();
        spyDelete.calls.reset();
        spyDelete.and.callFake(() => throwError(500));
        listComponent['onDialogConfirmationClosed']({confirm: true});
        expect(spyDelete).toHaveBeenCalled();
        expect(spyOnDeleteError).toHaveBeenCalled();
    });

});
