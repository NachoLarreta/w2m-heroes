import { TestBed } from '@angular/core/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { HeroesModule } from '@heroes';
import { Hero } from '@models';
import { DeleteConfirmationComponent } from './delete-confirmation.component';

describe('DeleteConfirmationComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HeroesModule,
        MatDialogModule
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {}
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: new Hero('1234', 'Test')
        }
      ]
    }).compileComponents();
  });

  it('should create the delete confirmation', () => {
    const fixture = TestBed.createComponent(DeleteConfirmationComponent);
    const deleteConfirmation = fixture.componentInstance;
    expect(deleteConfirmation).toBeTruthy();
  });
  
});
