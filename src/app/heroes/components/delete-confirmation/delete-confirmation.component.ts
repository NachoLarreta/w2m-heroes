import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Hero } from '@models';

@Component({
    selector: 'delete-confirmation',
    templateUrl: 'delete-confirmation.component.html',
    styleUrls: ['delete-confirmation.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeleteConfirmationComponent {

    constructor(public dialogRef: MatDialogRef<DeleteConfirmationComponent>,
        @Inject(MAT_DIALOG_DATA) public hero: Hero) {
    }

}
