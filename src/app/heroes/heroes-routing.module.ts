import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from '@heroes';

const routes: Routes = [
  {
    path: 'list',
    component: ListComponent
  },
  {
    path: 'create-update',
    loadChildren: () => import('./components/create-update/create-update.module').then(m => m.CreateUpdateModule)
  },
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HeroesRoutingModule {}
