import { TestBed } from '@angular/core/testing';
import { LoadingService } from '@services';
import { LoadingInterceptor } from './loading.interceptor';

describe('LoadingInterceptor', () => {

    let service: LoadingInterceptor;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                LoadingInterceptor,
                LoadingService
            ]
        });
        service = TestBed.inject(LoadingInterceptor);
    });

    it('To be created', () => {
        expect(service).toBeTruthy();
    });

});
