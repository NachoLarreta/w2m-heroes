// This is a dumb server, please take in consideration that it is for testing porposes only
const { v4: uuidv4 } = require('uuid');
const express = require('express');
const bodyParser = require('body-parser');
var cors = require('cors')

const app = express();
const port = 3000;

let heroes = [{ id: uuidv4(), name: 'Superman' }, { id: uuidv4(), name: 'Spiderman' }, { id: uuidv4(), name: 'Elastigirl' }, { id: uuidv4(), name: 'Antman' }];
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(bodyParser.raw());
app.use(cors())

app
    .get('/', (req, res) => {
        const id = req.query.id;
        const name = req.query.name;
        const pageSize = req.query.pageSize ? Number(req.query.pageSize) : 5;
        const pageIndex = req.query.pageIndex ? Number(req.query.pageIndex) : 1;
        if (id) {
            const heroe = heroes.length > 0 ? heroes.find(hero => hero.id === id) : null;
            return res.status(heroe ? 200 : 404).send(heroe);
        }
        if (name) {
            const heroesFiltered = heroes.length > 0 ? heroes.filter(hero => hero.name.toLowerCase().includes(name.toLowerCase())) : [];
            return res.status(200).send({elements: [...heroesFiltered].slice((pageIndex - 1) * pageSize, pageIndex * pageSize), length: heroesFiltered.length, pageSize, pageIndex});
        }
        return res.status(200).send({elements: [...heroes].slice((pageIndex - 1) * pageSize, pageIndex * pageSize), length: heroes.length, pageSize, pageIndex});
    })
    .put('/', function (req, res) {
        let hero = req.body;
        if (!hero) return res.status(400).send();
        const index = heroes.findIndex(heroAux => heroAux.id === hero.id);
        if (index < 0) return res.status(404).send();
        heroes[index].name = hero.name;
        return res.status(200).send();
    })
    .post('/', function (req, res) {
        let hero = req.body;
        if (!hero) return res.status(400).send();
        hero.id = uuidv4();
        heroes.push({id: hero.id, name: hero.name});
        return res.status(200).send(hero);
    })
    .delete('/', function (req, res) {
        const id = req.query.id;
        if (!id) return res.status(400).send();
        const index = heroes.findIndex(hero => hero.id === id);
        if (index < 0) return res.status(404).send();
        heroes = heroes.filter((hero, i) => i !== index);
        return res.status(200).send();
    });

app.listen(port, () => {
    console.log(`Dumb server listening at http://localhost:${port}`);
})